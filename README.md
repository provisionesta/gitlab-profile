Provisionesta is a namespace library of open source packages, projects, and tools for IT and Security teams at companies with up to ~10,000 users (using Okta and SaaS applications/platforms) to automate employee and contractor workforce Identity/IAM/RBAC REST API provisioning/deprovisioning for onboarding (joiner), just-in-time (JIT) access requests, job role changes (mover), offboarding (leaver), compliance changelog, and event dispatching. Some tools provide lightweight open source alternatives to commercial identity governance vendor products.

See the docs at [provisionesta.org](https://provisionesta.org) or in the `README.md` of each project to get started.

### Namespace

The projects in the Provisionesta namespace were created by [Jeff Martin](https://linkedin.com/in/jeffersonmmartin) as side projects to improve IT and Security automation as part of his day job on the [GitLab Identity Security](https://handbook.gitlab.com/handbook/security/identity/) team as we iterate on our homegrown [Identity Platform](https://handbook.gitlab.com/handbook/security/identity/platform/). 

The name Provisionesta was inspired by the concept of being a maestro or a provision-ista/istro (phonetically), but used `-esta` for proper Latin/Spanish grammar. Jeff wanted a single word to use as a namespace instead of his name to allow others to contribute as well.

See the [contributing](#contributing) to learn more.

### Langauge and Tech Stack

Most platform integrated packages are created using the [Laravel](https://laravel.com/docs) framework based on Jeff's 10+ years of experience with Laravel and 20+ years with PHP. Other languages including Golang, Python, and Ruby and associated frameworks provide a lot of programming functionality, however they are missing some of the breadth of batteries included features that we depend on using Laravel (PHP). 

Since Jeff provides coding mentorship for engineers that are just learning to code, we are focused on our mission that everyone can contribute and teaching others to contribute to and maintain our collective Identity Platform tools using our PHP code base. You can get started learning yourself at [provisionesta.org/training](https://provisionesta.org/training) and [provisionesta.org/contributing](https://provisionesta.org/contributing).

We welcome side project scripts that use Golang, Python, or Ruby. For maintainability and security reasons, we only accept PHP code that is a Composer package that is installable in the Laravel framework.

We also have Ansible and Terraform configuration-as-code packages. 

### Contributing

Each package is maintained with contributions by GitLab team members and we welcome contributions from the open source community, particularly those that work in HR, IT, or Security roles and have a passion for automation. 

See the [provisionesta.org/contributing](https://provisionesta.org/contributing) documentation for getting started instructions with learning Laravel and how our code is architected.

See the `CONTRIBUTING.md` in each project for developer environment configuration instructions.

We welcome new projects from other GitLab team members after an architecture and security review by Jeff Martin. To promote and link to other community projects, please open a merge request in the [provisionesta/awesome-provisioning-tools](https://gitlab.com/provisionesta/awesome-provisioning-tools) project.

### Open Source Licensing

All projects are licensed as [MIT](https://spdx.org/licenses/MIT.html). 

These packages are created and contributed to on GitLab company time as as part of our [mission](https://handbook.gitlab.com/handbook/company/mission/) to `enable everyone to contribute and co-create the software that powers our world`. You can learn more about our [open source](https://handbook.gitlab.com/handbook/engineering/open-source/) spirit with our [iteration](https://handbook.gitlab.com/handbook/values/#iteration) and [transparency](https://handbook.gitlab.com/handbook/values/#transparency) values.

The contributions from GitLab team members are focused on our internal business needs, and while we make a best effort to take a universal industry approach, our security practices may not cover all of your business, compliance, legal, or security requirements. 

We welcome contributions from the community (that's you!) that add additional features or coverage for your business requirements. You are also welcome to fork this project into a private repository if needed.

### Liability

Your comany is welcome to use these tools by cloning or forking the projects and reading the documentation to deploy in your environment. 

Please use at your own risk. As part of the [MIT license](https://spdx.org/licenses/MIT.html), GitLab does not provide any commercial liability, sales, services, or support related to these packages or open source projects. In other words, we do not provide guarantee of maintenance, legal liability, audit support, security incident response, or any other support while using these packages at your company. 

You can create an issue for any bugs that are encountered, however we do not provide an SLA or guarantee of resolution. For faster resolution of bugs, please try to locate the line of code that is causing the problem, create a merge request, and add details in the merge request about the problem so that the maintainers find the root cause faster.

We cannot provide security questionnaire or audit support. See [provisionesta.org/compliance](https://provisionesta.org/compliance) for documentation on control mappings that our maintainers manage that you can use as a starting point. 

These projects are not dependencies for the GitLab product or GitLab customers, and are simply used as internal tools for automating internal business processes at GitLab. In other words, it's just a supersized library of internal scripts used by IT and Security teams that we have open sourced.

### Conclusion

Please create an issue in the respective project for all technical related topics. For private inquiries, please email `provisionesta [at] jeffersonmartin [dot] com`.

We hope you enjoy, and invite you to contribute. 